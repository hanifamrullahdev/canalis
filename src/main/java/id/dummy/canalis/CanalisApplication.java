package id.dummy.canalis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CanalisApplication {

	public static void main(String[] args) {
		SpringApplication.run(CanalisApplication.class, args);
	}

}
